%% uArm MBS Parameters 
%% Base 
Base.mass = 1.886; 
%% Link1 (Shoulder)
Joint1.offset = [0 0 0.0732];

Link1.mass = 0.2141; 
Link1.offset = [0 0 -0.0732];
%% Link2 (Arm1)
Joint2.offset = [0.0132 0 0.0333];

Link2.mass = 0.026; 
Link2.offset = [-0.0132 0 -0.1056];

%% Link3 (Armx)
Joint3.offset = [0 0 0.14207];

Link3.mass = 0.023; 
Link3.offset = [-0.0132 0 -0.24767];
%% Link4 (Armx)
Joint4.offset = [0 0 0.14207];
Link4.mass = 0.003; 
Link4.offset = [-0.0132 0 -0.24767];
%% Link5 (Armx)
Joint5.offset = [-0.0215 0 0.05001];

Link5.mass = 0.003; 
Link5.offset = [0.0215 0 -0.1223];
%% Link6 (Armx)
Joint6.offset = [0.0132 0 0.0333];

Link6.mass = 0.003; 
Link6.offset = [-0.0132 0 -0.1056];
%% Link7 (Armx)
Joint7.offset = [-0.0455 0 -0.00301];

Link7.mass = 0.003; 
Link7.offset = [0.0323 0 -0.10258];
%% Link8 (Gripper)
Joint8.offset = [0.15852 0 0];

Link8.mass = 0.012; 
Link8.offset = [-0.17201 0 -0.24651];
%% Link9 (Armx)
Joint9.offset = [0.02741 0 0.02703];

Link9.mass = 0.003; 
Link9.offset = [-0.19941 0 -0.27471];
%% Joint4_9 (Armx)
Joint4_9.offset = [0.02741 0 0.02704];
Joint9_4.offset = [-0.1588 0 0];

%% Joint4_5 (Armx)
Joint4_5.offset = [-0.03472 0 0.01663];
Joint5_4.offset = [0 0 0.142];
%% Joint3_7 (Armx)
Joint3_7.offset = [-0.0456 0 -0.003];  %need to be comfirmed
Joint7_3.offset = [0 0 0.142];